

require('./bootstrap');

import Vue from 'vue';

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './components/App.vue';
import VueSweetalert2 from 'vue-sweetalert2'; 
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueAxios, axios);

const IndexUser = require('./components/IndexUser.vue').default
const CreateUser = require('./components/CreateUser.vue').default
const EditUser = require('./components/EditUser.vue').default

const routes = [
    {
        name: 'home',
        path: '/',
        component: IndexUser,
        props: true
    },
    {
        name: 'create',
        path: '/create',
        component: CreateUser
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditUser,
        props: true
    },

  ];

  const router = new VueRouter({
       mode: 'history',
       routes
    });
  const app = new Vue({
      el: "#app",
      router,
      render: h => h(App),
  });
