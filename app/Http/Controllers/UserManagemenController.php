<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserManagemenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return $user->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:users|max:30',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6|max:15',

        ]);

        if($validated){
            User::create([
                "name" => $request->name,
                "email" => $request->email,
                "password" => Hash::make($request['password']),
            ]);
            $msg = [
                'status' => true,
                'message' => 'User Add successfully'
            ];
     
            return response()->json($msg);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $validated = $request->validate([
            'name' => 'required|max:30',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6|max:15',

        ]);

        if($validated){
            $user->update([
                "name" => $request->name,
                "email" => $request->email,
                "password" => Hash::make($request['password']),
            ]);
            $msg = [
                'status' => true,
                'message' => 'User updated successfully'
            ];
     
            return response()->json($msg);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        $msg = [
            'status' => true,
            'message' => 'User deleted successfully'
        ];
 
        return response()->json($msg);
    }
}
